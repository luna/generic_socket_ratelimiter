# generic_socket_ratelimiter

use https://github.com/mariusae/trickle though

throttling requests to specific hosts at the connect() call level

this implements the token bucket algorithm in zig, storing a hashmap for each
address. default ratelimit is 1 connect every 10 seconds.

hostnames with many addresses won't be treated as a single thing.

if your clients use TCP keepalive, this won't be able to prevent request
spamming at a certain host.

really, you should use trickle.

quickly slapping together an `LD_PRELOAD`able library that puts custom logic
on top of an existing function was fun to make though.

```
zig build
env LD_PRELOAD=zig-out/lib/libgeneric_socket_ratelimiter.so curl amogus
```
